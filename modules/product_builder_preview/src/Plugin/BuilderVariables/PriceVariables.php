<?php

namespace Drupal\product_builder_preview\Plugin\BuilderVariables;

use Drupal\product_builder_preview\Plugin\BuilderVariablesPluginBase;

/**
 * Defines a couple of fields for PriceItem field.
 *
 * @BuilderVariables(
 *   id = "commerce_price",
 *   label = @Translation("Price"),
 * )
 */
class PriceVariables extends BuilderVariablesPluginBase {

  public function getFieldKeys() {
    return ['number', 'currency_code'];
  }

}
