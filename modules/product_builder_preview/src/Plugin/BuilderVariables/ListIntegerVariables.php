<?php

namespace Drupal\product_builder_preview\Plugin\BuilderVariables;

use Drupal\product_builder_preview\Plugin\BuilderVariablesPluginBase;

/**
 * Provides a 'List Integer' builder variables.
 *
 * @BuilderVariables(
 *   id = "list_integer",
 *   label = @Translation("List Integer"),
 * )
 */
class ListIntegerVariables extends BuilderVariablesPluginBase {

}
