<?php

/**
 * @file
 * Hooks specific to the "product_builder_preview" module.
 */

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;

/**
* Alter ajax response.
*
* @param array $form
* @param FormStateInterface $form_state
* @param AjaxResponse $ajax_response
*
*/
function hook_product_builder_preview_callback_alter(&$form, FormStateInterface $form_state, AjaxResponse $ajax_response) {
  $ajax_response->addCommand(new ReplaceCommand('#wrapper-field-size', $form['field_size']));
}

/**
 * Modification alter ajax response width BUNDLE_ID.
 *
 * @param array $form
 * @param FormStateInterface $form_state
 * @param AjaxResponse $ajax_response
 *
 */
function hook_product_builder_preview_callback_BUNDLE_ID_alter(&$form, FormStateInterface $form_state, AjaxResponse $ajax_response) {
  $ajax_response->addCommand(new ReplaceCommand('#wrapper-field-size', $form['field_size']));
}
