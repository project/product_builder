Demo Pizza Product (Builder)
===============

## Installation

Ensure that you have commerce module configured and install 'product_builder_demo_pizza' as a regular module. 

Once it is done, you can see new Commerce product type, Product Builder bundle and a Taxonomy vocabulary. To get your demo product follow these steps (steps will be removed once the process is automated in the product_builder_demo_pizza.install file):
1. Create taxonomy terms in 'Demo Topping' vocabulary. And upload background images from the assets folder in this module. I suggest using following (images have similar names):
    - Tomato
    - Salami
    - Kittens
2. Create attributes for 'Demo Pizza Size' set. Please, use following:
    - Title: 30cm, Width: 300, Height: 300
    - Title: 40cm, Width: 400, Height: 400
3. Create a Demo Pizza Product with two product variations:
    - Title: Pizza 30cm, Price: 10, Demo Pizza Size: 30cm, Background: pizza_background.png (see the assets folder)
    - Title: Pizza 40cm, Price: 15, Demo Pizza Size: 40cm, Background: pizza_background.png (see the assets folder)
4. Put 'product-builder-preview-element--demo-pizza-builder.html.twig' file into your theme templates folder.

```
{#
/**
 * @file
 * Theme override for a 'form' element.
 *
 * Available variables
 * - attributes: A list of HTML attributes for the wrapper element.
 * - children: The child elements of the form.
 *
 * @see template_preprocess_form()
 */
#}

<h2 class="pizza-name" style="text-align:center;">
  <b>
    {{ state_variables.name }}
  </b>
</h2>

<div class="pizza-base"  style="
      background: url('{{ state_variables.variation.field_background }}');
      background-size: cover;
      width: {{ state_variables.variation.attribute_demo_pizza_size.field_width }}px;
      height: {{ state_variables.variation.attribute_demo_pizza_size.field_height }}px;
      position:relative;
">

{% for topping in state_variables.field_demo_topping %}
  <div class="pizza-layer" style="
          background: url('{{ topping.field_demo_topping_background }}');
          background-size: cover;
          width: 100%;
          height: 100%;
          position:absolute;
          "></div>
{% endfor %}

</div>
```
