# Commerce Product Builder

The product builder module concept for the DrupalCamp Wroclaw 2018.

## Getting Started

Contains product_builder_preview submodule, which allows designing more or less usefull customized product preview.

If you want to start with the module faster you can install one of the demo modules (currently, there is only one module, so you don't have much of a choice). This demo builder is described here: https://zanzarra.com/blog/product-builder-part-1-custom-pizza-builder
See the modules/demo/pizza folder. Install this module and follow its README.md

## Caution

Beware, it's just a concept and does not ready for any kind of production use. 
